/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.models;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Thumbnail options model.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_ThumbnailOptions.Builder.class)
public abstract class ThumbnailOptions {
    @Nullable
    public abstract Thumbnail getDefault();

    @Nullable
    public abstract Thumbnail getMedium();
    
    @Nullable
    public abstract Thumbnail getHigh();

    @Nullable
    public abstract Thumbnail getStandard();

    @Nullable
    public abstract Thumbnail getMaxres();

    public static Builder builder() {
        return new AutoValue_ThumbnailOptions.Builder();
    }
    
    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setDefault(@Nullable Thumbnail defaultRes);

        public abstract Builder setMedium(@Nullable Thumbnail medium);

        public abstract Builder setHigh(@Nullable Thumbnail high);

        public abstract Builder setStandard(@Nullable Thumbnail standard);

        public abstract Builder setMaxres(@Nullable Thumbnail maxres);

        public abstract ThumbnailOptions build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_ThumbnailOptions_Thumbnail.Builder.class)
    public abstract static class Thumbnail {
        public abstract String getUrl();

        public abstract int getWidth();

        public abstract int getHeight();

        public static Builder builder() {
            return new AutoValue_ThumbnailOptions_Thumbnail.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setUrl(String url);

            public abstract Builder setWidth(int width);

            public abstract Builder setHeight(int height);

            public abstract Thumbnail build();
        }
    }
}
