/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.models;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * The localized information about the video. Title and description.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_LocalizedInfo.Builder.class)
public abstract class LocalizedInfo {
    public abstract String getTitle();

    @Nullable
    public abstract String getDescription();

    public static Builder builder() {
        return new AutoValue_LocalizedInfo.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setTitle(String title);

        public abstract Builder setDescription(@Nullable String desc);

        public abstract LocalizedInfo build();
    }
}
