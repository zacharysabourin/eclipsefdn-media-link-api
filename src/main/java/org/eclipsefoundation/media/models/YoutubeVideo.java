/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.models;

import java.time.ZonedDateTime;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Response object for a requested video.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_YoutubeVideo.Builder.class)
public abstract class YoutubeVideo {
    public abstract String getId();

    public abstract String getEtag();

    public abstract VideoSnippet getSnippet();

    public abstract PlayerInfo getPlayer();

    public static Builder builder() {
        return new AutoValue_YoutubeVideo.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(String id);

        public abstract Builder setEtag(String etag);

        public abstract Builder setSnippet(VideoSnippet snippet);

        public abstract Builder setPlayer(PlayerInfo player);

        public abstract YoutubeVideo build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubeVideo_VideoSnippet.Builder.class)
    public abstract static class VideoSnippet {

        public abstract ZonedDateTime getPublishedAt();

        public abstract String getChannelId();

        public abstract String getTitle();

        @Nullable
        public abstract String getDescription();

        public abstract ThumbnailOptions getThumbnails();

        public abstract String getChannelTitle();

        @Nullable
        public abstract List<String> getTags();

        @Nullable
        public abstract String getCategoryId();

        public abstract String getLiveBroadcastContent();

        public abstract String getDefaultLanguage();

        public abstract String getDefaultAudioLanguage();

        public abstract LocalizedInfo getLocalized();

        public static Builder builder() {
            return new AutoValue_YoutubeVideo_VideoSnippet.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            @JsonProperty("publishedAt")
            public abstract Builder setPublishedAt(ZonedDateTime date);

            @JsonProperty("channelId")
            public abstract Builder setChannelId(String channelId);

            public abstract Builder setTitle(String title);

            public abstract Builder setDescription(@Nullable String desc);

            public abstract Builder setThumbnails(ThumbnailOptions thumbnails);

            @JsonProperty("channelTitle")
            public abstract Builder setChannelTitle(String title);

            public abstract Builder setTags(@Nullable List<String> tags);

            @JsonProperty("categoryId")
            public abstract Builder setCategoryId(@Nullable String categoryId);

            @JsonProperty("liveBroadcastContent")
            public abstract Builder setLiveBroadcastContent(String content);

            @JsonProperty("defaultLanguage")
            public abstract Builder setDefaultLanguage(String language);

            @JsonProperty("defaultAudioLanguage")
            public abstract Builder setDefaultAudioLanguage(String audioLanguage);

            public abstract Builder setLocalized(LocalizedInfo info);

            public abstract VideoSnippet build();
        }
    }
}
