/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.config;

import java.util.Map;

import io.smallrye.config.ConfigMapping;

/**
 * A config class used to map media provider config properties such as the
 * managed channel names and api key. The API key is located in the
 * '/config/application/secret.properties' file.
 */
@ConfigMapping(prefix = "eclipse.media-provider.youtube")
public interface YoutubeMediaProviderConfig {

    Map<String, String> channels();

    String apiKey();
}
