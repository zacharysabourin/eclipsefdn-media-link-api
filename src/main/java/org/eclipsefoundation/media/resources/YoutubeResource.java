/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.resources;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.eclipsefoundation.media.models.YoutubePlaylist;
import org.eclipsefoundation.media.models.YoutubeVideo;
import org.eclipsefoundation.media.services.YoutubePlaylistService;
import org.eclipsefoundation.media.services.YoutubeVideoService;

/**
 * Resource class containing all Youtube media resources. Provides access to
 * managed channel video and playlist entities.
 */
@Path("/youtube")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class YoutubeResource {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static final String MANAGED_CHANNELS_URL = "/media/youtube/managed_channels";

    @Inject
    YoutubeMediaProviderConfig config;

    @Inject
    YoutubeVideoService videoService;

    @Inject
    YoutubePlaylistService playlistService;

    /**
     * Returns a 200 OK Response containing all YoutubeVideo entities matching the
     * list of provided ids. This endpoint will perform a best effort match against
     * all ids. If any ids are invalid or not found, it will still return all valid
     * entities.
     * Returns a 400 Bad Request if there are no 'ids' provided, or if all provided
     * 'ids' are not from managed channels.
     * 
     * @param ids The comma separated list of desired YoutubeVideo ids.
     * @return A 200 OK Response containing all YoutubeVideo entities matching the
     *         list of provided ids. A 400 Bad Request if there are no 'ids'
     *         provided, or if all provided 'ids' are not from managed channels.
     */
    @GET
    @Path("/videos")
    public Response getVideos(@QueryParam("ids") String ids) {

        // Ensure ids are present
        if (StringUtils.isBlank(ids)) {
            throw new BadRequestException("The ids are required to use this endpoint");
        }

        // Fetch all videos
        List<YoutubeVideo> videos = videoService.getVideosById(ids);
        if (videos.isEmpty()) {
            throw new BadRequestException(String.format(
                    "The videos: %s must be from a channel managed by this API. Please see %s for managed channel names.",
                    ids, MANAGED_CHANNELS_URL));
        }

        return Response.ok(videos).build();
    }

    /**
     * Returns a 200 OK Response containing a YoutubeVideo entity corresponding to
     * the desired 'videoID'. Returns a 400 Bad Request if the video id is not from
     * a managed channel.
     * 
     * @param id The given video id.
     * @return A 200 OK Response containing the desired video if available. A 400
     *         Bad Request Response if the video is not from a managed channel.
     */
    @GET
    @Path("/videos/{videoID}")
    public Response getVideo(@PathParam("videoID") String id) {

        // Fetch video
        Optional<YoutubeVideo> video = videoService.getVideoById(id);
        if (video.isEmpty()) {
            throw new BadRequestException(String.format(
                    "The video: %s must be from a channel managed by this API. Please see %s for managed channel names.",
                    id, MANAGED_CHANNELS_URL));
        }

        return Response.ok(video).build();
    }

    /**
     * Returns a 200 OK Response containing a list of all YoutubePlaylist entities
     * that match the given channel name, filtering for results after a given date.
     * The list can be empty if there are no playlists for the channel or if the
     * 'afterDate' provided is outside of the range of results. Returns a
     * 400 Bad Request if the channel param is missing or is not a managed channel,
     * or if the date is in an invalid format.
     * 
     * @param channel   The desired channel.
     * @param afterDate Return only results after this date.
     * @return A 200 OK Response containing a list of all YoutubePlaylist entities
     *         that match the given channel name, filtering for results after a
     *         given date. A 400 Bad Request if the channel param is missing or is
     *         not a managed channel.
     */
    @GET
    @Path("/playlists")
    public Response getPlaylists(@QueryParam("channel") String channel, @QueryParam("afterDate") String afterDate) {

        // Ensure channel is present
        if (StringUtils.isBlank(channel)) {
            throw new BadRequestException("The channel name is required to use this endpoint");
        }

        // Channel name must be EF managed channel
        if (!isTrackedChannel(channel)) {
            throw new BadRequestException(
                    "The channel:" + channel + "is not managed by this API. Please see " + MANAGED_CHANNELS_URL
                            + " for managed channel names.");
        }

        List<YoutubePlaylist> playlists = playlistService.getPlaylistsByChannel(config.channels().get(channel));

        // Filter by date if provided
        if (StringUtils.isNotBlank(afterDate)) {
            return getFilteredPlaylists(afterDate, playlists);
        }

        return Response.ok(playlists).build();
    }

    /**
     * Returns a 200 OK Response containing a Playlist entity that corresponds to
     * the 'playlistID' in the path param. Returns a 400 Bad Request if the playlist
     * id is not from a managed channel.
     * 
     * @param id The given playlist id.
     * @return A 200 OK Response containing the desired playlist if available. A 400
     *         Bad Request Response if the playlist is not from a managed channel.
     */
    @GET
    @Path("/playlists/{playlistID}")
    public Response getPlaylist(@PathParam("playlistID") String id) {
        Optional<YoutubePlaylist> playlist = playlistService.getPlaylistById(id);
        if (playlist.isEmpty()) {
            throw new BadRequestException(String.format(
                    "The playlist: %s must be from a channel managed by this API. Please see %s for managed channel names.",
                    id, MANAGED_CHANNELS_URL));
        }

        return Response.ok(playlist.get()).build();
    }

    /**
     * Returns a 200 OK Response containing the list of channels managed by the
     * application. The managed channels list is configurable by adding a new entry
     * under 'eclipse.media-provider.youtube.channels.<name>=<channel_id>'.
     * 
     * @return A 200 OK Response containing all managed channel names.
     */
    @GET
    @Path("/managed_channels")
    public Response getManagedChannels() {
        return Response.ok(config.channels().keySet()).build();
    }

    /**
     * Filters a playlist list by date. Filtering out playlists before the given
     * date. Throws BadRequestException if the date is in an invalid format.
     * 
     * @param afterDate The starting date range
     * @param videos    The list of playlists
     * @return The filtered playlist list as response or BadRequestException
     */
    private Response getFilteredPlaylists(String afterDate, List<YoutubePlaylist> playlists) {
        try {
            LocalDate localDate = LocalDate.parse(afterDate, FORMATTER);
            ZonedDateTime filterDate = localDate.atStartOfDay(ZoneId.systemDefault());

            // Filter for playlists after provided date.
            return Response.ok(playlists.stream()
                    .filter(p -> p.getSnippet().getPublishedAt().isAfter(filterDate)
                            || p.getSnippet().getPublishedAt().isEqual(filterDate))
                    .collect(Collectors.toList())).build();

        } catch (Exception ex) {
            throw new BadRequestException(String.format("Invalid date: %s", afterDate));
        }
    }

    /**
     * Checks the map of managed YT channels and returns true if the channel
     * exists, false if not.
     * 
     * @param channel the channel name
     * @return true if tracked, false if not
     */
    private boolean isTrackedChannel(String channelName) {
        return config.channels().keySet().stream().anyMatch(c -> c.equalsIgnoreCase(channelName));
    }
}
