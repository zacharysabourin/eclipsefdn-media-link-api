/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.precaches;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.ProcessingException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.core.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.media.api.YoutubeAPI;
import org.eclipsefoundation.media.api.models.YoutubeChannelResponse;
import org.eclipsefoundation.media.api.models.YoutubePlaylistItemResponse;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Precache provider for Youtube video ids.
 */
@Named("videos")
@ApplicationScoped
public class YoutubeVideoPrecacheProvider implements LoadingCacheProvider<String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubeVideoPrecacheProvider.class);

    private static final String PART = "contentDetails";

    // the max number of results that can be pulled from a YT video endpoint in a single call
    public static final Integer MAX_RESULTS_VIDEO_RESPONSE = 50;

    @Inject
    YoutubeMediaProviderConfig config;

    @RestClient
    YoutubeAPI api;

    @Inject
    CachingService cache;

    @Override
    public List<String> fetchData(ParameterizedCacheKey k) {
        try {
            LOGGER.info("Caching video ids for channel: {}", k.getId());

            // Get playlist id for channel's "all" playlist
            String playlistId = getChannelUploadsId(k.getId());

            // Get first page of results, using max amount
            YoutubePlaylistItemResponse response = api
                    .getItemsByPlaylistId(YoutubeRequestParams
                            .builder()
                            .setPart(PART)
                            .setPlaylistId(playlistId)
                            .setMaxResults(MAX_RESULTS_VIDEO_RESPONSE)
                            .setKey(config.apiKey())
                            .build());

            // Add each video id to our final list
            List<String> out = response
                    .getItems()
                    .stream()
                    .map(i -> i.getContentDetails().getVideoId())
                    .collect(Collectors.toList());

            // Iterate over every page of YT API results
            while (response.getNextPageToken() != null) {
                response = api
                        .getItemsByPlaylistId(YoutubeRequestParams
                                .builder()
                                .setPart(PART)
                                .setPageToken(response.getNextPageToken())
                                .setPlaylistId(playlistId)
                                .setMaxResults(MAX_RESULTS_VIDEO_RESPONSE)
                                .setKey(config.apiKey())
                                .build());

                // Add each additional video id to our final list
                out
                        .addAll(response
                                .getItems()
                                .stream()
                                .map(i -> i.getContentDetails().getVideoId())
                                .collect(Collectors.toList()));
            }

            return out;
        } catch (Exception e) {
            throw new ProcessingException(String.format("Error caching video ids for channel: %s", k.getId()), e);
        }
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }

    /**
     * Fetches the uploads playlist id for the given channel.
     * 
     * @param channelId The desired channel's id
     * @return A string containing the playlist id, or null.
     */
    private String getChannelUploadsId(String channelId) {
        LOGGER.debug("Fetching UploadID for channel: {}", channelId);
        Optional<YoutubeChannelResponse> response = cache
                .get(channelId, new MultivaluedMapImpl<>(), YoutubeChannelResponse.class,
                        () -> api
                                .getChannelInfo(YoutubeRequestParams
                                        .builder()
                                        .setPart(PART)
                                        .setId(channelId)
                                        .setKey(config.apiKey())
                                        .build()));
        if (response.isEmpty()) {
            throw new ProcessingException(String.format("Error fetching UploadID for channel: %s", channelId));
        }

        return response.get().getItems().get(0).getContentDetails().getRelatedPlaylists().getUploads();
    }
}
