/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.services;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.media.models.YoutubePlaylist;

/**
 * Fetches from the YT API for playlist data on a given channel or a
 * given id.
 */
public interface YoutubePlaylistService {

    /**
     * Calls the YT API and retreives a list containing all of a channel's
     * playlists.
     * 
     * @param channelId The id for the requested channel
     * @return A list with all the channel's playlists
     */
    List<YoutubePlaylist> getPlaylistsByChannel(String channelId);

    /**
     * Calls the YT API and retreives a single playlist.
     * 
     * @param playlistId The id for the requested playlist
     * @return An Optional containing the playlist entity if it exists.
     */
    Optional<YoutubePlaylist> getPlaylistById(String playlistId);
}
