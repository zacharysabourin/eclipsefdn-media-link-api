/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.services;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.media.models.YoutubeVideo;

/**
 * Fetches from the YT API for video data on a given channel or a
 * given id.
 */
public interface YoutubeVideoService {

    /**
     * Calls the YT API and retreives a list containg all of requested videos.
     * 
     * @param videoIds the ids for the requested videos
     * @return A list with all the requested videos
     */
    List<YoutubeVideo> getVideosById(String videoIds);

    /**
     * Calls the YT API and retreives a single video.
     * 
     * @param videoId the id for the requested playlist
     * @return An Optional containg the desired video entity if it exists
     */
    Optional<YoutubeVideo> getVideoById(String videoId);   
}
