/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.ProcessingException;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.LoggingHelper;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.core.service.LoadingCacheManager;
import org.eclipsefoundation.media.api.YoutubeAPI;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.api.models.YoutubeVideoResponse;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.eclipsefoundation.media.models.YoutubeVideo;
import org.eclipsefoundation.media.services.YoutubeVideoService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;

/**
 * Default implementation of YoutubeVideoService. Uses a loading cache video ids by channel. Also caches individual
 * videos using the EF CachingService.
 */
@Startup
@ApplicationScoped
public class DefaultYoutubeVideoService implements YoutubeVideoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultYoutubeVideoService.class);

    @Inject
    YoutubeMediaProviderConfig config;

    @RestClient
    YoutubeAPI api;

    @Inject
    LoadingCacheManager cacheManager;

    @Inject
    CachingService cache;

    @PostConstruct
    public void init() {
        // Pre-load cache
        config
                .channels()
                .values()
                .parallelStream()
                .forEach(s -> cacheManager
                        .getList(ParameterizedCacheKey
                                .builder()
                                .setId(s)
                                .setClazz(String.class)
                                .setParams(new MultivaluedMapImpl<>())
                                .build()));
    }

    @Override
    public List<YoutubeVideo> getVideosById(String videoIds) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Videos: {}", LoggingHelper.format(videoIds));
        }

        // Split video ids into list
        List<String> ids = Arrays.asList(videoIds.split("\\s{0,4},\\s{0,4}"));
        if (ids.isEmpty()) {
            Collections.emptyList();
        }

        // Fetch each video and add to list if present
        List<YoutubeVideo> videos = new ArrayList<>();
        ids.forEach(id -> getVideoById(id).ifPresent(videos::add));
        return videos;
    }

    @Override
    public Optional<YoutubeVideo> getVideoById(String videoId) {
        // Validate the id before making request to YT API
        if (!isValidVideoId(videoId)) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Invalid video id: {}", LoggingHelper.format(videoId));
            }
            return Optional.empty();
        }
        return fetchVideo(videoId);
    }

    /**
     * Fetches a video from the YT API and caches the result.
     * 
     * @param key the cache key/video id
     * @return A YoutubeVideo entity
     */
    private Optional<YoutubeVideo> fetchVideo(String videoId) {
        // save the sanitized version of the video ID for logging + exceptions
        String sanitizedVideoId = LoggingHelper.format(videoId);
        LOGGER.debug("Fetching Video with ID: {}", sanitizedVideoId);

        // Fetch fingle video from YT API
        Optional<YoutubeVideoResponse> response = cache
                .get(videoId, new MultivaluedMapImpl<>(), YoutubeVideoResponse.class, () -> api
                        .getVideosById(YoutubeRequestParams.builder().setId(videoId).setKey(config.apiKey()).build()));
        if (response.isEmpty()) {
            throw new ProcessingException(String.format("Error while loading video with id: %s", sanitizedVideoId));
        }

        return Optional.of(response.get().getItems().get(0));

    }

    /**
     * Iterates through the list of video ids and returns whether or not the requested videoId exists.
     * 
     * @param videoId the requestedc video id
     * @return true if match, false if not
     */
    private boolean isValidVideoId(String videoId) {
        List<String> videoIds;

        // For each channel id, check if id matches against cached list of video ids
        for (String channelId : config.channels().values()) {
            videoIds = cacheManager
                    .getList(ParameterizedCacheKey
                            .builder()
                            .setId(channelId)
                            .setClazz(String.class)
                            .setParams(new MultivaluedMapImpl<>())
                            .build());

            if (videoIds.stream().anyMatch(s -> s.equalsIgnoreCase(videoId))) {
                return true;
            }
        }
        return false;
    }
}
