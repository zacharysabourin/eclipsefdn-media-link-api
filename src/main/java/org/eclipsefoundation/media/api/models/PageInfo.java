/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Pagination information for incoming YT-API responses.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_PageInfo.Builder.class)
public abstract class PageInfo {

    @JsonProperty("totalResults")
    public abstract int getTotalResults();

    @JsonProperty("resultsPerPage")
    public abstract int getResultsPerPage();

    public static Builder builder() {
        return new AutoValue_PageInfo.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        @JsonProperty("totalResults")
        public abstract Builder setTotalResults(int total);

        @JsonProperty("resultsPerPage")
        public abstract Builder setResultsPerPage(int perPage);

        public abstract PageInfo build();
    }
}