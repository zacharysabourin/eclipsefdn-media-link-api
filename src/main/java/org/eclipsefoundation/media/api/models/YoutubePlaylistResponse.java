/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api.models;

import java.util.List;

import javax.annotation.Nullable;

import org.eclipsefoundation.media.models.YoutubePlaylist;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Response object for queries to the YT-API at '/playlists'.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_YoutubePlaylistResponse.Builder.class)
public abstract class YoutubePlaylistResponse {

    public abstract String getEtag();

    public abstract PageInfo getPageInfo();

    @Nullable
    @JsonProperty("nextPageToken")
    public abstract String getNextPageToken();

    @Nullable
    @JsonProperty("prevPageToken")
    public abstract String getPrevPageToken();

    public abstract List<YoutubePlaylist> getItems();

    public static Builder copy(YoutubePlaylistResponse response) {
        return new AutoValue_YoutubePlaylistResponse.Builder()
                .setEtag(response.getEtag())
                .setPageInfo(response.getPageInfo());
    }

    public static Builder builder() {
        return new AutoValue_YoutubePlaylistResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setEtag(String etag);

        @JsonProperty("pageInfo")
        public abstract Builder setPageInfo(PageInfo info);

        @JsonProperty("nextPageToken")
        public abstract Builder setNextPageToken(@Nullable String token);

        @JsonProperty("prevPageToken")
        public abstract Builder setPrevPageToken(@Nullable String token);

        public abstract Builder setItems(List<YoutubePlaylist> items);

        public abstract YoutubePlaylistResponse build();
    }
}
