/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api.models;

import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Response object for queries to the YT-API at '/channels'.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_YoutubeChannelResponse.Builder.class)
public abstract class YoutubeChannelResponse {

    public abstract String getEtag();

    public abstract PageInfo getPageInfo();

    public abstract List<Channel> getItems();

    public static Builder builder() {
        return new AutoValue_YoutubeChannelResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setEtag(String etag);

        @JsonProperty("pageInfo")
        public abstract Builder setPageInfo(PageInfo info);

        public abstract Builder setItems(List<Channel> items);

        public abstract YoutubeChannelResponse build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubeChannelResponse_Channel.Builder.class)
    public abstract static class Channel {

        public abstract String getEtag();

        public abstract String getId();

        public abstract ChannelContentDetails getContentDetails();

        public static Builder builder() {
            return new AutoValue_YoutubeChannelResponse_Channel.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setEtag(String etag);

            public abstract Builder setId(String id);

            @JsonProperty("contentDetails")
            public abstract Builder setContentDetails(ChannelContentDetails details);

            public abstract Channel build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubeChannelResponse_ChannelContentDetails.Builder.class)
    public abstract static class ChannelContentDetails {

        @JsonProperty("relatedPlaylists")
        public abstract RelatedPlaylists getRelatedPlaylists();

        public static Builder builder() {
            return new AutoValue_YoutubeChannelResponse_ChannelContentDetails.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            @JsonProperty("relatedPlaylists")
            public abstract Builder setRelatedPlaylists(RelatedPlaylists related);

            public abstract ChannelContentDetails build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubeChannelResponse_RelatedPlaylists.Builder.class)
    public abstract static class RelatedPlaylists {
        @Nullable
        public abstract String getFavorites();

        public abstract String getUploads();

        public static Builder builder() {
            return new AutoValue_YoutubeChannelResponse_RelatedPlaylists.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setFavorites(@Nullable String favorites);

            public abstract Builder setUploads(String uploads);

            public abstract RelatedPlaylists build();
        }
    }
}