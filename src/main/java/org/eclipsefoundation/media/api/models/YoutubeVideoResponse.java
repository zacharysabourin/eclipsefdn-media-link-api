/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api.models;

import java.util.List;

import org.eclipsefoundation.media.models.YoutubeVideo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Response object for queries to the YT-API at '/playlists'
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_YoutubeVideoResponse.Builder.class)
public abstract class YoutubeVideoResponse {

    public abstract String getEtag();

    public abstract PageInfo getPageInfo();

    public abstract List<YoutubeVideo> getItems();

    public static Builder copy(YoutubeVideoResponse response) {
        return new AutoValue_YoutubeVideoResponse.Builder()
                .setEtag(response.getEtag())
                .setPageInfo(response.getPageInfo());
    }

    public static Builder builder() {
        return new AutoValue_YoutubeVideoResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setEtag(String etag);

        @JsonProperty("pageInfo")
        public abstract Builder setPageInfo(PageInfo info);

        public abstract Builder setItems(List<YoutubeVideo> items);

        public abstract YoutubeVideoResponse build();
    }
}
