/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.resources;

import org.eclipsefoundation.media.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
class YoutubeResourceTest {

    public static final String YT_BASE_URL = "/youtube";

    public static final String VIDEOS_BASE_URL = YT_BASE_URL + "/videos";
    public static final String VIDEOS_CHANNEL_URL = VIDEOS_BASE_URL + "?channel={param1}";
    public static final String VIDEOS_CHANNEL_DATE_URL = VIDEOS_CHANNEL_URL + "&afterDate={param2}";
    public static final String VIDEOS_URL = VIDEOS_BASE_URL + "?ids={param1}";
    public static final String VIDEO_URL = VIDEOS_BASE_URL + "/{videoID}";

    public static final String PLAYLISTS_BASE_URL = YT_BASE_URL + "/playlists";
    public static final String PLAYLISTS_CHANNEL_URL = PLAYLISTS_BASE_URL + "?channel={param1}";
    public static final String PLAYLISTS_CHANNEL_DATE_URL = PLAYLISTS_CHANNEL_URL + "&afterDate={param2}";
    public static final String PLAYLIST_URL = PLAYLISTS_BASE_URL + "/{playlistID}";

    public static final String MANAGED_CHANNELS_URL = YT_BASE_URL + "/managed_channels";
    /*
     * GET ALL VIDEOS
     */
    public final static EndpointTestCase GET_VIDEOS_SUCCESS = TestCaseHelper.buildSuccessCase(
            VIDEOS_URL, new String[] { "video2, videoThree" }, SchemaNamespaceHelper.VIDEOS_SCHEMA_PATH);

    public final static EndpointTestCase GET_VIDEOS_SUCCESS_ONE_UNTRACKED = TestCaseHelper.buildSuccessCase(
            VIDEOS_URL, new String[] { "video2 , video69" }, SchemaNamespaceHelper.VIDEOS_SCHEMA_PATH);

    public final static EndpointTestCase GET_VIDEOS_NO_IDS = TestCaseHelper.buildBadRequestCase(
            VIDEOS_BASE_URL, new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public final static EndpointTestCase GET_VIDEOS_UNTRACKED_CHANNEL = TestCaseHelper.buildBadRequestCase(
            VIDEOS_URL, new String[] { "videoUno, videoDos" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public final static EndpointTestCase GET_VIDEOS_ONLY_COMMA = TestCaseHelper.buildBadRequestCase(
            VIDEOS_URL, new String[] { "," }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET VIDEO
     */
    public final static EndpointTestCase GET_VIDEO_BY_ID_SUCCESS = TestCaseHelper.buildSuccessCase(VIDEO_URL,
            new String[] { "video2" }, SchemaNamespaceHelper.VIDEO_SCHEMA_PATH);

    public final static EndpointTestCase GET_VIDEO_BY_ID_UNTRACKED_CHANNEL = TestCaseHelper.buildBadRequestCase(
            VIDEO_URL, new String[] { "videoUno" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET ALL PLAYLISTS
     */
    public final static EndpointTestCase GET_ALL_PLAYLISTS_SUCCESS = TestCaseHelper.buildSuccessCase(
            PLAYLISTS_CHANNEL_DATE_URL, new String[] { "eclipsefdn", "2015-04-17" },
            SchemaNamespaceHelper.PLAYLISTS_SCHEMA_PATH);

    public final static EndpointTestCase GET_ALL_PLAYLISTS_NO_CHANNEL = TestCaseHelper.buildBadRequestCase(
            PLAYLISTS_BASE_URL, new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public final static EndpointTestCase GET_ALL_PLAYLISTS_INVALID_CHANNEL = TestCaseHelper.buildBadRequestCase(
            PLAYLISTS_CHANNEL_URL, new String[] { "awfulyt" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public final static EndpointTestCase GET_ALL_PLAYLISTS_INVALID_DATE = TestCaseHelper.buildBadRequestCase(
            PLAYLISTS_CHANNEL_DATE_URL, new String[] { "eclipsefdn", "202-15-6" },
            SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /*
     * GET PLAYLIST
     */
    public final static EndpointTestCase GET_PLAYLIST_BY_ID_SUCCESS = TestCaseHelper.buildSuccessCase(PLAYLIST_URL,
            new String[] { "playlist1" }, SchemaNamespaceHelper.PLAYLIST_SCHEMA_PATH);

    public final static EndpointTestCase GET_PLAYLIST_BY_ID_UNTRACKED_CHANNEL = TestCaseHelper.buildBadRequestCase(
            PLAYLIST_URL, new String[] { "playlistUno" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    /**
     * MANAGED CHANNELS
     */
    public final static EndpointTestCase GET_MANAGED_CHANNELS_SUCCESS = TestCaseHelper.buildSuccessCase(MANAGED_CHANNELS_URL, new String[]{}, "");

    /*
     * GET VIDEOS
     */
    @Test
    void getVideos_success() {
        EndpointTestBuilder.from(GET_VIDEOS_SUCCESS).run();
    }

    @Test
    void getVideos_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_VIDEOS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getVideos_success_validSchema() {
        EndpointTestBuilder.from(GET_VIDEOS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getVideos_success_oneVideoNotFound() {
        EndpointTestBuilder.from(GET_VIDEOS_SUCCESS_ONE_UNTRACKED).run();
    }

    @Test
    void getVideos_success_oneVideoNotFound_validResponseFormat() {
        EndpointTestBuilder.from(GET_VIDEOS_SUCCESS_ONE_UNTRACKED).andCheckFormat().run();
    }

    @Test
    void getVideos_success_oneVideoNotFound_validSchema() {
        EndpointTestBuilder.from(GET_VIDEOS_SUCCESS_ONE_UNTRACKED).andCheckSchema().run();
    }

    @Test
    void getVideos_failure_noIds() {
        EndpointTestBuilder.from(GET_VIDEOS_NO_IDS).run();
    }

    @Test
    void getVideos_failure_noIds_validSchema() {
        EndpointTestBuilder.from(GET_VIDEOS_NO_IDS).andCheckSchema().run();
    }

    @Test
    void getVideos_failure_invalidChannel() {
        EndpointTestBuilder.from(GET_VIDEOS_UNTRACKED_CHANNEL).run();
    }

    @Test
    void getVideos_failure_invalidChannel_validSchema() {
        EndpointTestBuilder.from(GET_VIDEOS_UNTRACKED_CHANNEL).andCheckSchema().run();
    }

    @Test
    void getVideos_failure_onlyComma() {
        EndpointTestBuilder.from(GET_VIDEOS_ONLY_COMMA).run();
    }

    @Test
    void getVideos_failure_onlyComma_validSchema() {
        EndpointTestBuilder.from(GET_VIDEOS_ONLY_COMMA).andCheckSchema().run();
    }

    /*
     * GET VIDEO
     */
    @Test
    void getVideoByID_success() {
        EndpointTestBuilder.from(GET_VIDEO_BY_ID_SUCCESS).run();
    }

    @Test
    void getVideoByID_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_VIDEO_BY_ID_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getVideoByID_success_validSchema() {
        EndpointTestBuilder.from(GET_VIDEO_BY_ID_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getVideoByID_failure_untrackedChannel() {
        EndpointTestBuilder.from(GET_VIDEO_BY_ID_UNTRACKED_CHANNEL).run();
    }

    @Test
    void getVideoByID_failure_untrackedChannel_validSchema() {
        EndpointTestBuilder.from(GET_VIDEO_BY_ID_UNTRACKED_CHANNEL).andCheckSchema().run();
    }

    /*
     * GET ALL PLAYLISTS
     */
    @Test
    void getAllPlaylists_success() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_SUCCESS).run();
    }

    @Test
    void getAllPlaylists_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getAllPlaylists_success_validSchema() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getAllPlaylists_failure_noChannel() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_NO_CHANNEL).run();
    }

    @Test
    void getAllPlaylists_failure_noChannel_validSchema() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_NO_CHANNEL).andCheckSchema().run();
    }

    @Test
    void getAllPlaylists_failure_invalidDate() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_INVALID_DATE).run();
    }

    @Test
    void getAllPlaylists_failure_invalidDate_validSchema() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_INVALID_DATE).andCheckSchema().run();
    }

    @Test
    void getAllPlaylists_failure_invalidChannel() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_INVALID_CHANNEL).run();
    }

    @Test
    void getAllPlaylists_failure_invalidChannel_validSchema() {
        EndpointTestBuilder.from(GET_ALL_PLAYLISTS_INVALID_CHANNEL).andCheckSchema().run();
    }

    /*
     * GET PLAYLIST
     */
    @Test
    void getPlaylistByID_success() {
        EndpointTestBuilder.from(GET_PLAYLIST_BY_ID_SUCCESS).run();
    }

    @Test
    void getPlaylistByID_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_PLAYLIST_BY_ID_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getPlaylistByID_success_validSchema() {
        EndpointTestBuilder.from(GET_PLAYLIST_BY_ID_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getPlaylistByID_failure_untrackedChannel() {
        EndpointTestBuilder.from(GET_PLAYLIST_BY_ID_UNTRACKED_CHANNEL).run();
    }

    @Test
    void getPlaylistByID_failure_untrackedChannel_validSchema() {
        EndpointTestBuilder.from(GET_PLAYLIST_BY_ID_UNTRACKED_CHANNEL).andCheckSchema().run();
    }

    /**
     * MANAGED CHANNELS
     */
    @Test
    void getManagedChannels_success() {
        EndpointTestBuilder.from(GET_MANAGED_CHANNELS_SUCCESS).run();
    }
}
