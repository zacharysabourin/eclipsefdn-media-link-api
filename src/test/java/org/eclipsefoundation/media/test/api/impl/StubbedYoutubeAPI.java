/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.test.api.impl;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.media.api.YoutubeAPI;
import org.eclipsefoundation.media.api.models.PageInfo;
import org.eclipsefoundation.media.api.models.YoutubeChannelResponse;
import org.eclipsefoundation.media.api.models.YoutubeChannelResponse.Channel;
import org.eclipsefoundation.media.api.models.YoutubeChannelResponse.ChannelContentDetails;
import org.eclipsefoundation.media.api.models.YoutubeChannelResponse.RelatedPlaylists;
import org.eclipsefoundation.media.api.models.YoutubePlaylistItemResponse;
import org.eclipsefoundation.media.api.models.YoutubePlaylistItemResponse.ItemContentDetails;
import org.eclipsefoundation.media.api.models.YoutubePlaylistItemResponse.ItemSnippet;
import org.eclipsefoundation.media.api.models.YoutubePlaylistItemResponse.PlaylistItem;
import org.eclipsefoundation.media.api.models.YoutubePlaylistResponse;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.api.models.YoutubeVideoResponse;
import org.eclipsefoundation.media.models.LocalizedInfo;
import org.eclipsefoundation.media.models.PlayerInfo;
import org.eclipsefoundation.media.models.ThumbnailOptions;
import org.eclipsefoundation.media.models.YoutubePlaylist;
import org.eclipsefoundation.media.models.YoutubePlaylist.PlaylistSnippet;
import org.eclipsefoundation.media.models.YoutubeVideo;
import org.eclipsefoundation.media.models.YoutubeVideo.VideoSnippet;

import io.quarkus.test.Mock;

@Mock
@RestClient
@ApplicationScoped
public class StubbedYoutubeAPI implements YoutubeAPI {

    private static final YoutubePlaylistResponse EMPTY_PLAYLIST_RESPONSE = YoutubePlaylistResponse.builder()
            .setEtag("etag")
            .setPageInfo(PageInfo.builder()
                    .setTotalResults(0)
                    .setResultsPerPage(50)
                    .build())
            .setItems(Collections.emptyList())
            .build();

    private static final YoutubeVideoResponse EMPTY_VIDEO_RESPONSE = YoutubeVideoResponse.builder()
            .setEtag("etag")
            .setPageInfo(PageInfo.builder()
                    .setTotalResults(0)
                    .setResultsPerPage(50)
                    .build())
            .setItems(Collections.emptyList())
            .build();

    private List<YoutubePlaylistResponse> playlistResponses;
    private List<YoutubeVideoResponse> videoResponses;
    private List<YoutubeChannelResponse> channelResponses;
    private List<YoutubePlaylistItemResponse> playlistItemResponses;

    public StubbedYoutubeAPI() {
        this.playlistResponses = new ArrayList<>();
        this.playlistResponses.addAll(Arrays.asList(
                YoutubePlaylistResponse.builder()
                        .setEtag("faketag")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(50)
                                .setTotalResults(3)
                                .build())
                        .setItems(Arrays.asList(
                                YoutubePlaylist.builder()
                                        .setEtag("etag")
                                        .setId("playlistOne")
                                        .setSnippet(PlaylistSnippet.builder()
                                                .setChannelId("eclipseID")
                                                .setChannelTitle(
                                                        "Eclipse Foundation")
                                                .setDescription("A playlist")
                                                .setTitle("Playlist One")
                                                .setPublishedAt(ZonedDateTime
                                                        .of(2020, 03, 17, 12, 05, 00, 00, ZoneId.systemDefault()))
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A playlist")
                                                        .setTitle("Playlist One")
                                                        .build())
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Add this to your page")
                                                .build())
                                        .build(),
                                YoutubePlaylist.builder()
                                        .setEtag("etag")
                                        .setId("playlistTwo")
                                        .setSnippet(PlaylistSnippet.builder()
                                                .setChannelId("eclipseID")
                                                .setChannelTitle(
                                                        "Eclipse Foundation")
                                                .setDescription("A playlist")
                                                .setTitle("Playlist Two")
                                                .setPublishedAt(ZonedDateTime
                                                        .of(2021, 10, 8, 12, 05, 00, 00, ZoneId.systemDefault()))
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A playlist")
                                                        .setTitle("Playlist Two")
                                                        .build())
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Add this to your page")
                                                .build())
                                        .build(),
                                YoutubePlaylist.builder()
                                        .setEtag("etag")
                                        .setId("playlistThree")
                                        .setSnippet(PlaylistSnippet.builder()
                                                .setChannelId("eclipseID")
                                                .setChannelTitle(
                                                        "Eclipse Foundation")
                                                .setDescription("A playlist")
                                                .setTitle("Playlist Three")
                                                .setPublishedAt(ZonedDateTime
                                                        .of(2015, 5, 1, 16, 05, 00, 00, ZoneId.systemDefault()))
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A playlist")
                                                        .setTitle("PLaylist Three")
                                                        .build())
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Add this to your page")
                                                .build())
                                        .build()))
                        .build(),
                YoutubePlaylistResponse.builder()
                        .setEtag("unrealTag")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(50)
                                .setTotalResults(3)
                                .build())
                        .setItems(Arrays.asList(
                                YoutubePlaylist.builder()
                                        .setEtag("etag")
                                        .setId("playlist1")
                                        .setSnippet(PlaylistSnippet.builder()
                                                .setChannelId("jakartaID")
                                                .setChannelTitle(
                                                        "Jakarta EE")
                                                .setDescription("A playlist")
                                                .setTitle("Playlist 1")
                                                .setPublishedAt(ZonedDateTime
                                                        .of(2020, 03, 17, 12, 05, 00, 00, ZoneId.systemDefault()))
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A playlist")
                                                        .setTitle("Playlist 1")
                                                        .build())
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Add this to your page")
                                                .build())
                                        .build(),
                                YoutubePlaylist.builder()
                                        .setEtag("etag")
                                        .setId("playlist2")
                                        .setSnippet(PlaylistSnippet.builder()
                                                .setChannelId("jakartaID")
                                                .setChannelTitle(
                                                        "Jakarta EE")
                                                .setDescription("A playlist")
                                                .setTitle("Playlist 2")
                                                .setPublishedAt(ZonedDateTime
                                                        .of(2021, 10, 8, 12, 05, 00, 00, ZoneId.systemDefault()))
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A playlist")
                                                        .setTitle("Playlist 2")
                                                        .build())
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Add this to your page")
                                                .build())
                                        .build(),
                                YoutubePlaylist.builder()
                                        .setEtag("etag")
                                        .setId("playlist3")
                                        .setSnippet(PlaylistSnippet.builder()
                                                .setChannelId("jakartaID")
                                                .setChannelTitle(
                                                        "Jakarta EE")
                                                .setDescription("A playlist")
                                                .setTitle("Playlist 3")
                                                .setPublishedAt(ZonedDateTime
                                                        .of(2015, 5, 1, 16, 05, 00, 00, ZoneId.systemDefault()))
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A playlist")
                                                        .setTitle("Playlist 3")
                                                        .build())
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Add this to your page")
                                                .build())
                                        .build()))
                        .build()));

        this.videoResponses = new ArrayList<>();
        this.videoResponses.addAll(Arrays.asList(
                YoutubeVideoResponse.builder()
                        .setEtag("etag")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(50)
                                .setTotalResults(3)
                                .build())
                        .setItems(Arrays.asList(
                                YoutubeVideo.builder()
                                        .setEtag("etag")
                                        .setId("videoOne")
                                        .setSnippet(VideoSnippet.builder()
                                                .setPublishedAt(ZonedDateTime.now())
                                                .setChannelId("eclipseID")
                                                .setTitle("Video One")
                                                .setDescription("A Video description")
                                                .setChannelTitle("Eclipse Foundation")
                                                .setCategoryId("some category")
                                                .setLiveBroadcastContent("none")
                                                .setDefaultLanguage("en")
                                                .setDefaultAudioLanguage("en")
                                                .setThumbnails(ThumbnailOptions.builder().build())
                                                .setLocalized(LocalizedInfo.builder()
                                                        .setDescription("A description")
                                                        .setTitle("Video One")
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Put this on your page")
                                                .build())
                                        .build(),
                                YoutubeVideo.builder()
                                        .setEtag("etag")
                                        .setId("videoTwo")
                                        .setSnippet(VideoSnippet.builder()
                                                .setPublishedAt(ZonedDateTime
                                                        .now())
                                                .setChannelId("eclipseID")
                                                .setTitle("Video Two")
                                                .setDescription("A Video description")
                                                .setChannelTitle(
                                                        "Eclipse Foundation")
                                                .setCategoryId("some category")
                                                .setLiveBroadcastContent("none")
                                                .setDefaultLanguage("en")
                                                .setDefaultAudioLanguage("en")
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A description")
                                                        .setTitle("Video Two")
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Put this on your page")
                                                .build())
                                        .build(),
                                YoutubeVideo.builder()
                                        .setEtag("etag")
                                        .setId("videoThree")
                                        .setSnippet(VideoSnippet.builder()
                                                .setPublishedAt(ZonedDateTime
                                                        .now())
                                                .setChannelId("eclipseID")
                                                .setTitle("Video Three")
                                                .setDescription("A Video description")
                                                .setChannelTitle(
                                                        "Eclipse Foundation")
                                                .setCategoryId("some category")
                                                .setLiveBroadcastContent("none")
                                                .setDefaultLanguage("en")
                                                .setDefaultAudioLanguage("en")
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A description")
                                                        .setTitle("Video Three")
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Put this on your page")
                                                .build())
                                        .build()))
                        .build(),
                YoutubeVideoResponse.builder()
                        .setEtag("etag")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(50)
                                .setTotalResults(3)
                                .build())
                        .setItems(Arrays.asList(
                                YoutubeVideo.builder()
                                        .setEtag("etag")
                                        .setId("video1")
                                        .setSnippet(VideoSnippet.builder()
                                                .setPublishedAt(ZonedDateTime.now())
                                                .setChannelId("jakartaID")
                                                .setTitle("Video 1")
                                                .setDescription("A Video description")
                                                .setChannelTitle("Jakarta EE")
                                                .setCategoryId("some category")
                                                .setLiveBroadcastContent("none")
                                                .setDefaultLanguage("en")
                                                .setDefaultAudioLanguage("en")
                                                .setThumbnails(ThumbnailOptions.builder().build())
                                                .setLocalized(LocalizedInfo.builder()
                                                        .setDescription("A description")
                                                        .setTitle("Video 1")
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Put this on your page")
                                                .build())
                                        .build(),
                                YoutubeVideo.builder()
                                        .setEtag("etag")
                                        .setId("video2")
                                        .setSnippet(VideoSnippet.builder()
                                                .setPublishedAt(ZonedDateTime
                                                        .now())
                                                .setChannelId("jakartaID")
                                                .setTitle("Video 2")
                                                .setDescription("A Video description")
                                                .setChannelTitle(
                                                        "Jakarta EE")
                                                .setCategoryId("some category")
                                                .setLiveBroadcastContent("none")
                                                .setDefaultLanguage("en")
                                                .setDefaultAudioLanguage("en")
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A description")
                                                        .setTitle("Video 2")
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Put this on your page")
                                                .build())
                                        .build(),
                                YoutubeVideo.builder()
                                        .setEtag("etag")
                                        .setId("video3")
                                        .setSnippet(VideoSnippet.builder()
                                                .setPublishedAt(ZonedDateTime
                                                        .now())
                                                .setChannelId("jakartaID")
                                                .setTitle("Video 3")
                                                .setDescription("A Video description")
                                                .setChannelTitle(
                                                        "Jakarta EE")
                                                .setCategoryId("some category")
                                                .setLiveBroadcastContent("none")
                                                .setDefaultLanguage("en")
                                                .setDefaultAudioLanguage("en")
                                                .setThumbnails(ThumbnailOptions
                                                        .builder()
                                                        .build())
                                                .setLocalized(LocalizedInfo
                                                        .builder()
                                                        .setDescription("A description")
                                                        .setTitle("Video 3")
                                                        .build())
                                                .build())
                                        .setPlayer(PlayerInfo.builder()
                                                .setEmbedHtml("Put this on your page")
                                                .build())
                                        .build()))
                        .build()));

        this.channelResponses = new ArrayList<>();
        this.channelResponses.addAll(Arrays.asList(
                YoutubeChannelResponse.builder()
                        .setEtag("etag")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(5)
                                .setTotalResults(1)
                                .build())
                        .setItems(Arrays.asList(
                                Channel.builder()
                                        .setEtag("983475nfkja")
                                        .setId("eclipseID")
                                        .setContentDetails(ChannelContentDetails.builder()
                                                .setRelatedPlaylists(RelatedPlaylists.builder()
                                                        .setUploads("eclipseAll")
                                                        .build())
                                                .build())
                                        .build()))
                        .build(),
                YoutubeChannelResponse.builder()
                        .setEtag("etag")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(5)
                                .setTotalResults(1)
                                .build())
                        .setItems(Arrays.asList(
                                Channel.builder()
                                        .setEtag("984275hsdf")
                                        .setId("jakartaID")
                                        .setContentDetails(ChannelContentDetails.builder()
                                                .setRelatedPlaylists(RelatedPlaylists.builder()
                                                        .setUploads("jakartaAll")
                                                        .build())
                                                .build())
                                        .build()))
                        .build()));

        this.playlistItemResponses = new ArrayList<>();
        this.playlistItemResponses.addAll(Arrays.asList(
                YoutubePlaylistItemResponse.builder()
                        .setEtag("sadf890u34jhk")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(5)
                                .setTotalResults(1)
                                .build())
                        .setItems(Arrays.asList(
                                PlaylistItem.builder()
                                        .setEtag("etag")
                                        .setId("sadofia098hr34r")
                                        .setSnippet(ItemSnippet.builder()
                                                .setPlaylistId("eclipseAll")
                                                .build())
                                        .setContentDetails(ItemContentDetails.builder()
                                                .setVideoId("videoOne")
                                                .build())
                                        .build(),
                                PlaylistItem.builder()
                                        .setEtag("etag")
                                        .setId("89437hgajsg")
                                        .setSnippet(ItemSnippet.builder()
                                                .setPlaylistId("eclipseAll")
                                                .build())
                                        .setContentDetails(ItemContentDetails.builder()
                                                .setVideoId("videoTwo")
                                                .build())
                                        .build(),
                                PlaylistItem.builder()
                                        .setEtag("etag")
                                        .setId("9034tjnlksdngjif")
                                        .setSnippet(ItemSnippet.builder()
                                                .setPlaylistId("eclipseAll")
                                                .build())
                                        .setContentDetails(ItemContentDetails.builder()
                                                .setVideoId("videoThree")
                                                .build())
                                        .build()))
                        .build(),
                YoutubePlaylistItemResponse.builder()
                        .setEtag("3498dfjklgnrt")
                        .setPageInfo(PageInfo.builder()
                                .setResultsPerPage(5)
                                .setTotalResults(1)
                                .build())
                        .setItems(Arrays.asList(
                                PlaylistItem.builder()
                                        .setEtag("etag")
                                        .setId("4928hgfdkjgnwr")
                                        .setSnippet(ItemSnippet.builder()
                                                .setPlaylistId("jakartaAll")
                                                .build())
                                        .setContentDetails(ItemContentDetails.builder()
                                                .setVideoId("video1")
                                                .build())
                                        .build(),
                                PlaylistItem.builder()
                                        .setEtag("etag")
                                        .setId("afg095jtlkgn")
                                        .setSnippet(ItemSnippet.builder()
                                                .setPlaylistId("jakartaAll")
                                                .build())
                                        .setContentDetails(ItemContentDetails.builder()
                                                .setVideoId("video2")
                                                .build())
                                        .build(),
                                PlaylistItem.builder()
                                        .setEtag("etag")
                                        .setId("8923429804thg4hfkjasdbg")
                                        .setSnippet(ItemSnippet.builder()
                                                .setPlaylistId("jakartaAll")
                                                .build())
                                        .setContentDetails(ItemContentDetails.builder()
                                                .setVideoId("video3")
                                                .build())
                                        .build()))
                        .build()));
    }

    @Override
    public YoutubePlaylistResponse getPlaylistsByChannel(YoutubeRequestParams params) {
        return playlistResponses.stream()
                .filter(p -> p.getItems().get(0).getSnippet().getChannelId().equalsIgnoreCase(params.getChannelId()))
                .findFirst().orElse(EMPTY_PLAYLIST_RESPONSE);
    }

    @Override
    public YoutubeVideoResponse getVideosById(YoutubeRequestParams params) {

        // Find the VideoResponse that contains the requested id
        YoutubeVideoResponse matchingResponse = videoResponses.stream()
                .filter(r -> r.getItems().stream().anyMatch(v -> v.getId().equalsIgnoreCase(params.getId())))
                .findFirst().orElse(EMPTY_VIDEO_RESPONSE);

        if (matchingResponse.equals(EMPTY_VIDEO_RESPONSE)) {
            return EMPTY_VIDEO_RESPONSE;
        }

        // Get the requested video
        YoutubeVideo matchingVideo = matchingResponse.getItems().stream()
                .filter(i -> i.getId().equalsIgnoreCase(params.getId())).findFirst().orElse(null);

        return YoutubeVideoResponse.copy(matchingResponse).setItems(Arrays.asList(matchingVideo)).build();
    }

    @Override
    public YoutubePlaylistItemResponse getItemsByPlaylistId(YoutubeRequestParams params) {
        return playlistItemResponses.stream()
                .filter(c -> c.getItems().get(0).getSnippet().getPlaylistId().equalsIgnoreCase(params.getPlaylistId()))
                .findFirst().orElse(null);
    }

    @Override
    public YoutubeChannelResponse getChannelInfo(YoutubeRequestParams params) {
        return channelResponses.stream()
                .filter(c -> c.getItems().get(0).getId().equalsIgnoreCase(params.getId()))
                .findFirst().orElse(null);
    }
}
