openapi: "3.1.0"
info:
  version: 1.0.0
  title: Media Link API
  description: Access information on Eclipse Foundation video media (like youtube).
  license:
    name: Eclipse Public License - 2.0
    url: https://www.eclipse.org/legal/epl-2.0/
servers:
  - url: https://api.eclipse.org/media
    description: Production endpoint for the media link API

paths:
  /youtube/videos:
    parameters:
      - name: ids
        in: query
        description: A comma separated list of video ids
        required: true
        schema:
          type: string
    get:
      summary: Requires a comma separated ids query parameter, returns multiple videos
      description: Requires a comma separated ids query parameter, returns all videos from a given channel
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Videos"
        400:
          description: Bad Request - Missing query param, or no videos are from managed channel
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        500:
          description: Error while retrieving data

  /youtube/videos/{videoID}:
    parameters:
      - name: videoID
        in: path
        description: The video id
        required: true
        schema:
          type: string
    get:
      summary: A single video
      description: Returns a single video that exists for a configured user
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Video"
        400:
          description: Bad Request - video not from a managed channel
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        500:
          description: Error while retrieving data

  /youtube/playlists:
    parameters:
      - name: channel
        in: query
        description: The channel name
        required: true
        schema:
          type: string
      - name: afterDate
        in: query
        description: The starting date range
        required: false
        schema:
          type: string
    get:
      summary: A channel's playlists
      description: Requires a channel query parameter, returns multiple playlist definitions
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Playlists"
        400:
          description: Bad Request - Missing query param or invalid date
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        404:
          description: Not Found
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        500:
          description: Error while retrieving data

  /youtube/playlists/{playlistID}:
    parameters:
      - name: playlistID
        in: path
        description: The playlist id
        required: true
        schema:
          type: string
    get:
      summary: A single playlist
      description: Returns a single playlist that exists for a configured user.
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Playlist"
        400:
          description: Bad Request - playlist not from a managed channel
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        500:
          description: Error while retrieving data

  /youtube/managed_channels:
    get:
      summary: A List of channel names
      description: Returns a list of all managed channels. Used to get specific channel naming syntax to use other endpoints.
      responses:
        200:
          description: Success
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
        500:
          description: Error while retrieving data

components:
  schemas:
    Videos:
      type: array
      items:
        $ref: "#/components/schemas/Video"

    Video:
      type: object
      properties:
        etag:
          type: string
          description: Placeholder
        id:
          type: string
          description: The video id
        snippet:
          $ref: "#/components/schemas/VideoSnippet"
        player:
          $ref: "#/components/schemas/PlayerInfo"

    Playlists:
      type: array
      items:
        $ref: "#/components/schemas/Playlist"

    Playlist:
      type: object
      properties:
        etag:
          type: string
          description: Placeholder
        id:
          type: string
          description: The playlist id
        snippet:
          $ref: "#/components/schemas/PlaylistSnippet"
        player:
          $ref: "#/components/schemas/PlayerInfo"

    VideoSnippet:
      type: object
      properties:
        published_at:
          type: string
          description: The publish date and time
        channel_id:
          type: string
          description: The channel id
        title:
          type: string
          description: The video title
        description:
          oneOf:
            - type: string
            - type: "null"
          description: The video description
        thumbnails:
          $ref: "#/components/schemas/ThumbnailOptions"
        channel_title:
          type: string
          description: the channel title
        tags:
          oneOf:
            - type: array
            - type: "null"
          items:
            type: string
            description: video tags
        category_id:
          oneOf:
            - type: string
            - type: "null"
          description: the category id
        live_broadcast_content:
          type: string
          description: The live broadcast content
        default_language:
          type: string
          description: the default language code
        localized:
          $ref: "#/components/schemas/LocalizedInfo"
        default_audio_language:
          type: string
          description: The default audio language

    PlaylistSnippet:
      type: object
      properties:
        published_at:
          type: string
          description: The publish date and time
        channel_id:
          type: string
          description: The channel id
        title:
          oneOf:
            - type: string
            - type: "null"
          description: The playlist title
        description:
          type: string
          description: The playlist description
        thumbnails:
          $ref: "#/components/schemas/ThumbnailOptions"
        channel_title:
          type: string
          description: the channel title
        localized:
          $ref: "#/components/schemas/LocalizedInfo"

    PlayerInfo:
      type: object
      properties:
        embed_html:
          type: string
          description: The HTML required to embed the video on a page

    ThumbnailOptions:
      type: object
      properties:
        default:
          oneOf:
            - $ref: "#/components/schemas/Thumbnail"
            - type: "null"
        medium:
          oneOf:
            - $ref: "#/components/schemas/Thumbnail"
            - type: "null"
        high:
          oneOf:
            - $ref: "#/components/schemas/Thumbnail"
            - type: "null"
        standard:
          oneOf:
            - $ref: "#/components/schemas/Thumbnail"
            - type: "null"
        maxres:
          oneOf:
            - $ref: "#/components/schemas/Thumbnail"
            - type: "null"

    Thumbnail:
      type: object
      properties:
        url:
          type: string
          description: The thumbnail URL
        width:
          type: integer
          description: the thumbnail width
        height:
          type: integer
          description: the thumbnail height

    LocalizedInfo:
      type: object
      properties:
        title:
          type: string
          description: The localized title
        description:
          oneOf:
            - type: string
            - type: "null"
          description: the localized description

    Error:
      type: object
      properties:
        status_code:
          type: integer
          description: HTTP response code
        message:
          type: string
          description: Message containing error information
        url:
          oneOf:
            - type: string
            - type: "null"
          description: The URL
